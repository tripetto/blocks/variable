/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    Slots,
    Str,
    affects,
    definition,
    editor,
    insertVariable,
    isVariable,
    lookupVariable,
    makeMarkdownSafe,
    pgettext,
    populateVariables,
    tripetto,
} from "@tripetto/builder";
import { TMode } from "../../runner/text/mode";

/** Assets */
import ICON from "../../../assets/text.svg";

@tripetto({
    type: "condition",
    legacyBlock: true,
    context: PACKAGE_NAME,
    identifier: `${PACKAGE_NAME}:text`,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:variable", "Text match");
    },
})
export class VariableTextCondition extends ConditionBlock {
    readonly allowMarkdown = true;

    @definition
    @affects("#name")
    mode: TMode = "exact";

    @definition
    @affects("#name")
    match?: string;

    @definition
    ignoreCase?: boolean;

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        const slot = this.slot;

        if (slot instanceof Slots.Text) {
            const wrap = (s: string) =>
                (s &&
                    (this.mode === "contains" ||
                        this.mode === "not-contains" ||
                        this.mode === "starts" ||
                        this.mode === "ends") &&
                    `_${s}_`) ||
                s;
            const match: string =
                (isVariable(this.match)
                    ? lookupVariable(this, this.match)?.label &&
                      `@${this.match}`
                    : this.match &&
                      wrap(
                          makeMarkdownSafe(Str.replace(this.match, "\n", "↵"))
                      )) || "\\_\\_";

            switch (this.mode) {
                case "exact":
                case "not-exact":
                    return `@${slot.id} ${
                        this.mode === "not-exact" ? "\u2260" : "="
                    } ${match}`;
                case "contains":
                case "not-contains":
                case "starts":
                case "ends":
                    return `@${slot.id} ${
                        this.mode === "not-contains"
                            ? pgettext("block:variable", "does not contain")
                            : this.mode === "starts"
                            ? pgettext("block:variable", "starts with")
                            : this.mode === "ends"
                            ? pgettext("block:variable", "ends with")
                            : pgettext("block:variable", "contains")
                    } ${match}`;
                case "defined":
                    return `@${slot.id} ${pgettext(
                        "block:variable",
                        "not empty"
                    )}`;
                case "undefined":
                    return `@${slot.id} ${pgettext("block:variable", "empty")}`;
            }
        }

        return this.type.label;
    }

    get title() {
        return this.node?.label;
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            title: pgettext("block:variable", "Compare mode"),
            controls: [
                new Forms.Radiobutton<TMode>(
                    [
                        {
                            label: pgettext("block:variable", "Text matches"),
                            value: "exact",
                        },
                        {
                            label: pgettext(
                                "block:variable",
                                "Text does not match"
                            ),
                            value: "not-exact",
                        },
                        {
                            label: pgettext("block:variable", "Text contains"),
                            value: "contains",
                        },
                        {
                            label: pgettext(
                                "block:variable",
                                "Text does not contain"
                            ),
                            value: "not-contains",
                        },
                        {
                            label: pgettext(
                                "block:variable",
                                "Text starts with"
                            ),
                            value: "starts",
                        },
                        {
                            label: pgettext("block:variable", "Text ends with"),
                            value: "ends",
                        },
                        {
                            label: pgettext(
                                "block:variable",
                                "Text is not empty"
                            ),
                            value: "defined",
                        },
                        {
                            label: pgettext("block:variable", "Text is empty"),
                            value: "undefined",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "exact")
                ).on((mode: Forms.Radiobutton<TMode>) => {
                    form.visible(
                        mode.value !== "defined" && mode.value !== "undefined"
                    );

                    switch (mode.value) {
                        case "exact":
                            form.title = pgettext(
                                "block:variable",
                                "If text matches"
                            );
                            break;
                        case "not-exact":
                            form.title = pgettext(
                                "block:variable",
                                "If text does not match"
                            );
                            break;
                        case "contains":
                            form.title = pgettext(
                                "block:variable",
                                "If text contains"
                            );
                            break;
                        case "not-contains":
                            form.title = pgettext(
                                "block:variable",
                                "If text does not contain"
                            );
                            break;
                        case "starts":
                            form.title = pgettext(
                                "block:variable",
                                "If text starts with"
                            );
                            break;
                        case "ends":
                            form.title = pgettext(
                                "block:variable",
                                "If text ends with"
                            );
                            break;
                    }

                    if (textControl.isInteractable) {
                        textControl.focus();
                    }
                }),
            ],
        });

        const isVar = (this.match && isVariable(this.match)) || false;
        const variables = populateVariables(
            this,
            undefined,
            isVar ? this.match : undefined,
            false,
            this.slot?.id
        );
        const textControl = new Forms.Text(
            "multiline",
            !isVar ? this.match : ""
        )
            .label(pgettext("block:variable", "Use fixed text"))
            .action("@", insertVariable(this, "exclude"))
            .transformation(
                (this.slot instanceof Slots.Text && this.slot.transformation) ||
                    "none"
            )
            .autoFocus()
            .enter(this.editor.close)
            .escape(this.editor.close)
            .on((input) => {
                if (input.isFormVisible && input.isVisible) {
                    this.match = input.value;
                }
            });
        const variableControl = new Forms.Dropdown(
            variables,
            isVar ? this.match : ""
        )
            .label(pgettext("block:variable", "Use value of"))
            .width("full")
            .on((variable) => {
                if (variable.isFormVisible && variable.isObservable) {
                    this.match = variable.value || undefined;
                }
            });

        const form = this.editor
            .form({
                title: pgettext("block:variable", "If text matches"),
                controls: [
                    new Forms.Radiobutton<"text" | "variable">(
                        [
                            {
                                label: pgettext("block:variable", "Text"),
                                value: "text",
                            },
                            {
                                label: pgettext("block:variable", "Value"),
                                value: "variable",
                                disabled: variables.length === 0,
                            },
                        ],
                        isVar ? "variable" : "text"
                    ).on((type) => {
                        textControl.visible(type.value === "text");
                        variableControl.visible(type.value === "variable");

                        if (textControl.isInteractable) {
                            textControl.focus();
                        }
                    }),
                    textControl,
                    variableControl,
                    new Forms.Checkbox(
                        pgettext("block:variable", "Ignore case"),
                        Forms.Checkbox.bind(this, "ignoreCase", undefined, true)
                    ),
                ],
            })
            .visible(this.mode !== "defined" && this.mode !== "undefined");
    }
}
