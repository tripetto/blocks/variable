/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    castToBoolean,
    condition,
    isFilledString,
    tripetto,
} from "@tripetto/runner";
import { TMode } from "./mode";

@tripetto({
    type: "condition",
    legacyBlock: true,
    identifier: `${PACKAGE_NAME}:boolean`,
})
export class VariableBooleanCondition extends ConditionBlock<{
    readonly mode: TMode;
    readonly value?: string;
}> {
    @condition
    verify(): boolean {
        const booleanSlot = this.valueOf<boolean>();

        if (booleanSlot) {
            switch (this.props.mode) {
                case "true":
                    return (
                        booleanSlot.hasValue && castToBoolean(booleanSlot.value)
                    );
                case "false":
                    return (
                        booleanSlot.hasValue &&
                        !castToBoolean(booleanSlot.value)
                    );
                case "equal":
                case "not-equal":
                    // eslint-disable-next-line no-case-declarations
                    const value =
                        (isFilledString(this.props.value) &&
                            this.variableFor(this.props.value)) ||
                        undefined;
                    return (
                        (value &&
                            (booleanSlot.hasValue
                                ? castToBoolean(booleanSlot.value)
                                : undefined) ===
                                (value.hasValue
                                    ? castToBoolean(value.value)
                                    : undefined)) ||
                        false
                    );
                case "defined":
                    return booleanSlot.hasValue;
                case "undefined":
                    return !booleanSlot.hasValue;
            }
        }

        return false;
    }
}
