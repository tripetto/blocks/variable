/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import { mountNamespace } from "@tripetto/runner";

mountNamespace(PACKAGE_NAME);
